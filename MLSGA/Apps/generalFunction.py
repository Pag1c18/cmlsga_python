# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-
"""
Created on Mon Nov 12 20:17:44 2018

@author: Thomas Savasta
"""
"""******************* General functions *************"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from collections import OrderedDict
import random
from operator import itemgetter

def varbounds(nbcab):
    varbound = []
    for i in range(nbcab):
        var_i= [(0,1), (-1,1), (0.01,1), (0.01,1),(0.01,1),(0.01,1), (0.01,1)]
        for n in var_i:
            varbound.append(n)
    return varbound 

def vargeneration(num):
    var = []
    for i in range(num):
        var.append(random.random())
        var.append((-1)**random.randint(1,2)*random.random())
        var.append(1*random.random())
        var.append(random.random())
        var.append(random.random())
        var.append(random.random())
        var.append(random.random())
    return var

def interpol_section_points(offsetData, targ):
        z_data =[]
        y_data = []
        for i in range(len(offsetData)):
            y_data.append(abs(offsetData[i][2]))
            z_data.append(abs(offsetData[i][1]))
        zinterpol_points = []
        yinterpol_points = []
        for i in range(len(z_data)-1):
            if targ >= z_data[i] and targ <= z_data[i+1]:
                zinterpol_points.append([z_data[i], z_data[i+1]])
                yinterpol_points.append([y_data[i], y_data[i+1]])
        if len(zinterpol_points) == 0 : ytarg = 0
        else: ytarg = yinterpol_points[0][0]+(targ - zinterpol_points[0][0] )*((yinterpol_points[0][1]-yinterpol_points[0][0])/(zinterpol_points[0][1] - zinterpol_points[0][0]))
        return ytarg

def integral_trap(zdata, ydata):
    "returns the trapezoid intergration of a data set" 
    area = 0
    for i in range(len(zdata)-1):
        area_trap = ((zdata[i + 1] - zdata[i])*0.5*(ydata[i] + ydata[i + 1]))
        area += area_trap
    return area

def Max_beam(lines_plan):
    max_beams = []
    for i in range(len(lines_plan)):
        section = lines_plan[i]
        max_offset = max(section,  key=itemgetter(2))
        max_beams.append(max_offset[2])
    max_beam = max(max_beams)
    return max_beam
    
def Maxsurf_lines_plan(data):
    data.reverse()
    lines_plan = []
    for i in range(len(data)):
        section = sorted(data[i],key=itemgetter(1))
        lines_plan.append(section)
    for i in range(0,len(lines_plan)):
        if len(lines_plan[i]) == 0: del lines_plan[i]
    return lines_plan

def linesPlanData(filename):
    "returns the trformated lines plan data (.lhf) from data file" 
    data = np.loadtxt(filename, delimiter = ',', skiprows=1)
    sec_nb = int(data[0][0])
    count = 2
    Lines_data = []
    for j in range(sec_nb):
            section_j = []
            nb = int(data[count][0])
            for i in range(nb):
                section_j.append([data[count-1][0],data[count+i+1][0],data[count+i+1][1]])
            Lines_data.append(section_j)
            count = count + nb +2
    return Lines_data

def sortDeckCab(var, cabs, deck_num):
    if deck_num ==0:
        cabDeckName = cabs['lower deck']
        start = 0
        end = len(cabDeckName)
        cabxdeck = var[start:start+end*7]
    elif deck_num == 1:
        cabDeckName = cabs['main deck']
        start = len(cabs['lower deck'])*7
        end = len(cabDeckName)*7
        cabxdeck = var[start:start+end]
    elif deck_num == 2:
        cabDeckName = cabs['upper deck']
        start = (len(cabs['lower deck'])+len(cabs['main deck']))*7
        end = len(cabDeckName)*7
        cabxdeck = var[start:start+end]
    return (cabDeckName,cabxdeck)

def heaviside (u):
        if u < 0:
            r = 0
        elif u == 0:
            r = 0.5
        else:
            r = 1
        return r
    
def calcDistance(p1,p2):
    dx = abs(p1[0]-p2[0])
    dy = abs(p1[1]-p2[1])
    dist = (dx**2+dy**2)**0.5
    return (dist)

def findCentroid(cabx):
    "finds and retruns the centroid coordiantes of a cabin" 
    Cx = cabx[0]+cabx[2]*0.5
    # Cy stab
    W11 = cabx[1]+cabx[3]
    W12 = cabx[1]+cabx[5]
    W21 = cabx[1]+cabx[4]
    W22 = cabx[1]+cabx[6]
    # Cy port
    y = 0.5*(0.5*(W11 + W12)+0.5*(W21 + W22))
    Cy = y
    return (Cx, Cy)


def sortDistCab(nbDecks):
    cabins = OrderedDict()
    if nbDecks == 1 : 
        cabins.update({'lower deck':[('aft','Machinery'),('aft','Tender'),("Owner's room","Owner's bathroom"),('guest cabin',"guest's bathroom"),('Crew cabin',"Crew's bathroom")]})
    if nbDecks == 2 : 
        cabins.update({'lower deck':[('aft','Machinery'),('aft','Tender'),('Crew cabin',"Crew's bathroom")]})
        cabins.update({'main deck':[("Owner's room","Owner's bathroom"),('guest cabin',"guest's bathroom")]})
    if nbDecks == 3 : 
        cabins.update({'lower deck':[('aft','Machinery'),('aft','Tender'),('Crew cabin',"Crew's bathroom")]})
        cabins.update({'main deck':[("Owner's room","Owner's bathroom"),('guest cabin',"guest's bathroom")]})
    return cabins 

def cabinArea(cabx):
    return cabx[2] * 0.5*(cabx[4] - cabx[5] + cabx[3] - cabx[6])

def deckShape(floor, linesPlanData, plot = False):
    deckLength = abs(linesPlanData[0][0][0]-linesPlanData[-1][0][0])
    x = []
    y = []
    for sec in range(len(linesPlanData)):
        x.append(abs(linesPlanData[sec][0][0]-0.5*deckLength))
        y.append(interpol_section_points(linesPlanData[sec],floor))
    x.append(0)
    y.append(0)
    deck = np.array([x,y])
    if plot:
        plt.plot(x,y)
        plt.axis('equal')
    return (deck)

def cabinVarSortDraw(cabnum, variables):
    nbVar = 7
    x = variables
    cabVar = []
    for i in range(nbVar):
        cabVar.append(x[nbVar*(cabnum)+i])
    return cabVar

def section_area(section, waterline):
    y = []
    z = []
    k = 0
    while section[k][1] < waterline and k<len(section):
        y.append(section[k][2]) 
        z.append(section[k][1])
        k+=1
    area = integral_trap(z, y)
    return area

def CB(linesplan,waterline):
    section_x = []
    section_areas = []
    section_moments = []
    for i in range(len(linesplan)):
        section = linesplan[i]
        x = section[0][0]
        section_x.append(x)
        area = section_area(section, waterline)
        section_areas.append(area)
    for i in range(len(section_x)-1):
        l = [section_x[i], section_x[i+1]]
        a = [section_areas[i], section_areas[i+1]]
        vol = integral_trap(l, a)
        moment = vol * sum(l) * 0.5
        section_moments.append(moment)
    displaced_volume = integral_trap(section_x, section_areas)
    #print('nnnnnnnn ',displaced_volume*1025)
    LCB_miship = sum(section_moments)/displaced_volume
    return (LCB_miship,0)

def cabin_generation(LOA,double_bedroom_nb,single_bedroom_nb,masters_bedroom_nb,crew_room_nb,crew_recreational_area_nb,saloon_nb,galley_nb,dining_room_nb,office_nb,bridge_nb,gym_nb,tender_nb,machinery):
    name_row = ['Standard area', 'Standard max aspect ratio', 'Mass per unit area']
    cabins = pd.DataFrame(index = name_row)
    #cabins.set_index(['Name', 'Standard area', 'Standard aspect ratio'])
    if double_bedroom_nb != 0:
        for i in range(double_bedroom_nb):
            name = 'Double room {}'.format(i+1)
            area = (0.1833+0.2847) * LOA + (8.2756- 0.6325)
            aspect_ratio = 1.5
            mass = 17.45
            cabins[name] = [area, aspect_ratio, mass]
    if single_bedroom_nb != 0:
        for i in range(single_bedroom_nb):
            name = 'Single room {}'.format(i+1)
            area = (0.3462+0.1082) * LOA + (1.031- 0.5891)
            aspect_ratio = 1.5
            mass = 18.30
            cabins[name] = [area, aspect_ratio, mass]
    if masters_bedroom_nb != 0:
        for i in range(masters_bedroom_nb):
            name = 'Masters room {}'.format(i+1)
            area = (0.8238+0.4684 ) * LOA + (0.3471- 5.641)
            aspect_ratio = 2
            mass = 24.55
            cabins[name] = [area, aspect_ratio, mass]
    if crew_room_nb != 0:
        for i in range(crew_room_nb):
            name = 'Crew room {}'.format(i+1)
            #area = (0.3462+0.1082) * LOA + (1.031- 0.5891)
            if LOA < 60:
                area = 13
            if LOA > 60:
                area = 18
            aspect_ratio = 1.5
            mass = 22.3
            cabins[name] = [area, aspect_ratio, mass]
    if crew_recreational_area_nb != 0:
        for i in range(crew_recreational_area_nb):
            name = 'Crew recreational area {}'.format(i+1)
            if LOA<60:
                area = 1.1637*LOA - 20.554
            if LOA>60:
                area = 50
            aspect_ratio = 1.5
            mass = 10
            cabins[name] = [area, aspect_ratio, mass]
    if saloon_nb != 0:
        for i in range(saloon_nb):
            name = 'Saloon {}'.format(i+1)
            area = 2.9164 * LOA - 49.02
            aspect_ratio = 2
            mass = 25.
            cabins[name] = [area, aspect_ratio, mass]
    if galley_nb != 0:
        for i in range(galley_nb):
            name = 'Galley {}'.format(i+1)
            area = 0.6299 * LOA - 8.3969
            aspect_ratio = 2
            mass = 30.
            cabins[name] = [area, aspect_ratio, mass]
    if dining_room_nb != 0:
        for i in range(dining_room_nb):
            name = 'Dining room {}'.format(i+1)
            area = 1.016*LOA - 19.46
            aspect_ratio = 2.5
            mass = 18.30
            cabins[name] = [area, aspect_ratio, mass]
    if office_nb != 0:
        for i in range(office_nb):
            name = 'Office {}'.format(i+1)
            area = 0.1239 * LOA + 1.75
            aspect_ratio = 1.5
            mass = 15.
            cabins[name] = [area, aspect_ratio, mass]
    if bridge_nb != 0:
        for i in range(bridge_nb):
            name = 'Bridge {}'.format(i+1)
            area = 0.6488 * LOA - 9.146
            aspect_ratio = 4
            mass = 25.
            cabins[name] = [area, aspect_ratio, mass]
    if gym_nb != 0:
        for i in range(gym_nb):
            name = 'Gym {}'.format(i+1)
            area = 2.9164 * LOA - 49.02
            aspect_ratio = 2
            mass = 18.30
            cabins[name] = [area, aspect_ratio, mass]
    if tender_nb != 0:
        for i in range(tender_nb):
            name = 'Tender'.format(i+1)
            if LOA<=60:
                area = 0.4061 * np.exp(0.1172 * LOA)
            if LOA>60:
                area = 250
            aspect_ratio = 2.5
            mass = 110.
            cabins[name] = [area, aspect_ratio, mass]
    if machinery != 0:
        for i in range(machinery):
            name = 'Machinery'.format(i+1)
            area = 1.5324 * LOA - 13.827
            aspect_ratio = 2.5
            mass = 150.5
            cabins[name] = [area, aspect_ratio, mass]
    return cabins

def deck_sort_cabin(LOA, deck_nb, master_pos, double_bedroom_nb,single_bedroom_nb,masters_bedroom_nb,crew_room_nb,crew_recreational_area_nb,saloon_nb,galley_nb,dining_room_nb,office_nb,bridge_nb,gym_nb,tender_nb,machinery):
    """Assigns cabins to deck"""
    if LOA < 60:
        if deck_nb == 1:
            return  cabin_generation(LOA,double_bedroom_nb,single_bedroom_nb,masters_bedroom_nb,crew_room_nb,crew_recreational_area_nb,saloon_nb,galley_nb,dining_room_nb,office_nb,bridge_nb,gym_nb,tender_nb,machinery),[]
        if deck_nb == 2:
            if master_pos == 1:
                lower_deck = cabin_generation(LOA,double_bedroom_nb,single_bedroom_nb,masters_bedroom_nb,crew_room_nb,crew_recreational_area_nb,0,0,0,0,0,0,tender_nb,machinery)
                main_deck = cabin_generation(LOA,0,0,0,0,0,saloon_nb,galley_nb,dining_room_nb,office_nb,bridge_nb,gym_nb,0,0)
            if master_pos == 2:
                lower_deck = cabin_generation(LOA,double_bedroom_nb,single_bedroom_nb,0,crew_room_nb,crew_recreational_area_nb,0,0,0,0,0,0,tender_nb,machinery)
                main_deck = cabin_generation(LOA,0,0,masters_bedroom_nb,0,0,saloon_nb,galley_nb,dining_room_nb,office_nb,bridge_nb,gym_nb,0,0)
        return lower_deck,main_deck
    if LOA > 60:
        if deck_nb == 2:
            lower_deck = cabin_generation(LOA,0,single_bedroom_nb,0,crew_room_nb,crew_recreational_area_nb,0,0,0,0,0,gym_nb,tender_nb,machinery)
            main_deck = cabin_generation(LOA,double_bedroom_nb,0,masters_bedroom_nb,0,0,saloon_nb,galley_nb,dining_room_nb,office_nb,bridge_nb,0,0,0)
            return lower_deck,main_deck
    else: 
        print('Wrong number of decks')