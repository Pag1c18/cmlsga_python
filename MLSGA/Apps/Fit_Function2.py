import math
from  Layout2D import *

class Function(object):
    """description of class"""
    def __init__(self, fname, varsn, objsn, consn):
        self.name_func = fname;
        self.num_vars = varsn;
        self.num_objs = objsn;
        self.num_cons = consn;
        self.bound_min = [];
        self.bound_max = [];
        
class Layout(Function):
    def __init__(self):
        Function.__init__(self, "Layout", nb*7 + corridor_nb, obj_nb, 0)
        self.Bound_Set();
        
    def Bound_Set(self):
        prebounds = varbounds(nb)
        for i in range(0,len(prebounds)):
            self.bound_max.append(prebounds[i][1]);
            self.bound_min.append(prebounds[i][0]);
        for k in range(0,corridor_nb):
            self.bound_max.append(1);
            self.bound_min.append(-1);
        
    def Fitness_C(self, code):
        fitness = Layout2D(code, decks, plot=False)
        return fitness
    
    def Plot_PF(self,size):
        Real_PF = [];

        for i in range (0,size):
            temp_f1 = i/(size-1.);
            temp_f2 = 1- math.sqrt(temp_f1);

            PF_val = [temp_f1, temp_f2]
            Real_PF.append(list(PF_val));

        return Real_PF
