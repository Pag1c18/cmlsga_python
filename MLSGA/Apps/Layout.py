# -*- coding: utf-8 -*-
"""
Spyder Editor

@author: Thomas Savasta
"""
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from collections import OrderedDict
from generalFunction import *

"""******************* Layout class *************"""
class TwoDlayout:
    def __init__(self,row_variables, corridor_var, LOA, Cabins,deck_number, cabinFlow,floor, bulkheads, linesPlan, waterline):
        self.specs = OrderedDict()
        #Ship/deck ***********************************************************
        self.LOA = LOA
        self.linesPlan = linesPlan                                              #list Line plan data
        self.max_beam = Max_beam(self.linesPlan)
        self.floor = floor                                                      #Height of deck from baseline
        self.deck_number = deck_number
        self.deckshape = self.deckShape(floor, linesPlan, plot = False)         #(x,y) coordinates of deck
        self.wl = waterline                                                     # design waterline from baseline
        #Cabins characteristics **********************************************
        self.cabType = list(Cabins.columns)                                       
        self.cabsizes = list(Cabins.loc['Standard area'])#[27,7,7,34,16,12,7,10]#[27,14,8,23,16,16,16]       #Standard areas of cabin types 
        self.cabaspectR = list(Cabins.loc['Standard max aspect ratio'])#[2,2,2,2,1.5,1.5,1.5,1.5] # [3,3,4,3,3,3,3]        #Standard aspect ratio of cabin types 
        self.cabmass = list(Cabins.loc['Mass per unit area'])#[6,2,2,4,2,2,0.5,3]                                      #Standard mass of cabin types                                                        #length overall 
        self.nbCab = len(self.cabType)                                               #number of cabin on deck 
        self.cabinFlow = cabinFlow                                              #list of tuples with cabins to be close to each other e.g.[['Machienery','aft']]                                         #List of cabin types present on deck
        #Other ****************************************************************
        self.bulkheads = [pos * LOA for pos in bulkheads]                                              #list of bulhead position 
        self.corridor = [corridor_var[i]*self.max_beam for i in range(len(corridor_var))]
        self.specs.update({'LOA': self.LOA})
        self.specs.update({'Number of Cabins': self.nbCab})
        # Variables
        self.row_variables = row_variables                                      # input variables from GA
        self.varPerCab = int(len(row_variables))/(self.nbCab)                   #number of var per cabin
        self.variables = []                                                     # list of list of cabin variables (in caterisan coordinates)
        [self.triVar(i) for i in range(self.nbCab)]                             # tramsforms input from GA into cartesian coordiantes 
        self.cabin_remove_overlap()                                             #shrinks cabins to avoid overlap
        self.cabin_extend_beam()                                                #changes cabins aspect ratio 

    """**** Layout Utils  ****"""
    def cabinVarSort(self, cabnum):
        """find the corresponding variable list of a cabin given its index"""
        return self.variables[cabnum]
            
    def triVar(self, cabnum):
        """Converts normalized GA variables into the cabins coordiantes"""
        nbVar = int(self.varPerCab)
        x = self.row_variables
        cabVar = []
        Var_i= []
        for i in range(nbVar):
            Var_i.append(x[nbVar*(cabnum)+i])
        if len(self.bulkheads) != 0:
            cabVar.append(Var_i[0]*(self.deckLength-min(self.bulkheads))+min(self.bulkheads))
            blims = []
            for b in range(len(self.bulkheads)):
                if self.bulkheads[b]>cabVar[0]: blims.append(self.bulkheads[b])
                else: blims.append(self.deckLength)
            lim = min(blims)
        else: 
            cabVar.append(Var_i[0]*self.deckLength)
            lim = self.deckLength
        L = Var_i[2]*(lim-cabVar[0])
        B1 = self.deckOffset(cabVar[0], self.deckshape)
        B2 = self.deckOffset(cabVar[0]+L, self.deckshape)
        cabVar.append(Var_i[1]*min(B1,B2))
        cabVar.append(L)
        cabVar.append(Var_i[3]*(B1-cabVar[1]))
        cabVar.append(Var_i[4]*(B2-cabVar[1]))
        cabVar.append(-1*Var_i[5]*(B1+cabVar[1]))
        cabVar.append(-1*Var_i[6]*(B2+cabVar[1]))
        self.variables.append(cabVar)
        return cabVar
    
    def cabin_extend_beam(self):
        """ extends cabin width to deck if no other cabin is blocking the way
        ensures cabin respect a certain apect ratio"""
        for c in range(self.nbCab):
            cabx = self.variables[c]
            up = 0
            bot = 0
            for i in range(0, self.nbCab):
                if i != c:
                    cabx2 =  self.variables[i]
                    #check B2
                    if (round(cabx2[0],5)<round(cabx[0]+cabx[2],5) and round(cabx[0]+cabx[2],5)<round(cabx2[0]+cabx2[2],5)):
                        if cabx[1]<cabx2[1]: up+=+1
                        if cabx[1]>cabx2[1]: bot+=+1
                    if (round(cabx2[0],5)<round(cabx[0],5) and round(cabx[0],5)<round(cabx2[0]+cabx2[2],5)):
                        if cabx[1]<cabx2[1]: up+=+1
                        if cabx[1]>cabx2[1]: bot+=+1
                    if ((round(cabx[0],5)<round(cabx2[0],5) and round(cabx[0]+cabx[2],5) >= round(cabx2[0]+cabx2[2],5))
                    or  (round(cabx2[0],5)<round(cabx[0],5) and round(cabx2[0]+cabx2[2],5) >= round(cabx[0]+cabx[2],5))):
                        if cabx[1]<cabx2[1]: up+=+1
                        if cabx[1]>cabx2[1]: bot+=+1
            B1 = self.deckOffset(cabx[0], self.deckshape)
            B2 = self.deckOffset(cabx[0]+cabx[2], self.deckshape)
            if up == 0 :
                cabx[4] = B2 - cabx[1]
                cabx[3] = B1 - cabx[1]
            if bot == 0 :
                cabx[5] = -(B1 + cabx[1])
                cabx[6] = -(B2 + cabx[1])
            # aspect ratio
            ar  =  (0.5 * (cabx[3]+abs(cabx[5]) + abs(cabx[6]) + cabx[4])) - self.cabaspectR[c]*cabx[2]
            if ar > 0:
                ratioW = (cabx[3]+abs(cabx[5]))/(abs(cabx[6]) + cabx[4])
                ratioB1 = cabx[3]/(abs(cabx[5]) + cabx[3])
                ratioB2 = cabx[4]/(abs(cabx[6]) + cabx[4])
                changeB1 = ratioW * ar
                changeB2 = ar/ratioW
                changeB1_up = ratioB1*changeB1
                changeB1_bot = changeB1*(1-ratioB1)
                changeB2_up = ratioB2*changeB2
                changeB2_bot = changeB2*(1-ratioB2)
                cabx[3] += - changeB1_up
                cabx[5] += changeB1_bot
                cabx[4] += - changeB2_up
                cabx[6] += changeB2_bot
            for i in range(len(self.corridor)):
                if len(self.bulkheads) !=0:
                    if self.deck_number>0: lim = self.deckLength
                    else : lim = self.bulkheads[1]
                    if cabx[0] < lim:
                        if cabx[1]<self.corridor[i]:
                            if cabx[1]+cabx[3]>self.corridor[i]:
                                cabx[3] = self.corridor[i]-cabx[1]
                            if cabx[1]+cabx[4]>self.corridor[i]:
                                cabx[4] = self.corridor[i]-cabx[1]
                        if cabx[1]>self.corridor[i]:
                            if cabx[1]+cabx[5]<self.corridor[i]:
                                cabx[5] = -abs(cabx[1]-self.corridor[i])
                            if cabx[1]+cabx[6]<self.corridor[i]:
                                cabx[6] = -abs(cabx[1]-self.corridor[i])
            self.variables[c] = cabx
    
    
    def cabin_remove_overlap(self):
        """ reduced cabin varbibles in any direction to avoid overlap with other cabin """
        for i in range(len(self.variables)):
            cabx = self.variables[i]
            overlap = self.overLapConst(i)
            cabx[2] +=  - overlap[0]
            cabx[4] +=  - overlap[1]
            cabx[3] +=  - overlap[1]
            cabx[5] = cabx[5] + overlap[2]
            cabx[6] = cabx[6] + overlap[2]

            if cabx[3]<0: cabx[3]= 0.001
            if cabx[4]<0: cabx[4]= 0.001
            if cabx[6]>0: cabx[6]= -0.001
            if cabx[5]>0: cabx[5]= -0.001
            self.variables[i] = cabx
    
    
    def calcDistanceIndex(self, cab1, cab2):
        """calculates the distance between 2 cabins, the inputs cab1 and cab2
        are the index of the two cabin"""
        distance = 0
        if (cab1 == -1) and (cab2 != -1):
            cabx = self.cabinVarSort(cab2)
            if cabx[0]+cabx[2] == self.deckLength: distance = 0
            else : distance = abs(cabx[0]+cabx[2]-self.deckLength)
        if (cab2 == -1) and (cab1 != -1):
            cabx = self.cabinVarSort(cab1)
            if cabx[0]+cabx[2] == self.deckLength: distance = 0
            else : distance = abs(cabx[0]+cabx[2]-self.deckLength)
#        if (cab1 == -2) and (cab2 != -2):
#            cabx = self.cabinVarSort(cab2)
#            if cabx[0] == min(self.bulkheads): distance = 0
#            else : distance = abs(cabx[0]-min(self.bulkheads))
#        if (cab2 == -2) and (cab1 != -2):
#            cabx = self.cabinVarSort(cab1)
#            if cabx[0] == min(self.bulkheads): distance = 0
#            else : distance = abs(cabx[0]-min(self.bulkheads))
        else:
            cabx1 = self.cabinVarSort(cab1)
            cabx2 = self.cabinVarSort(cab2)
            if ( cabx1[0] + cabx1[2] == cabx2[0] or cabx2[0] + cabx2[2] == cabx1[0]
    			or cabx1[1] + cabx1[3] == cabx2[1] + cabx2[5] or cabx1[1] + cabx1[3] == cabx2[1] + cabx2[6]
              or cabx1[1] + cabx1[4] == cabx2[1] + cabx2[5] or cabx1[1] + cabx1[4] == cabx2[1] + cabx2[6]
              or cabx1[1] + cabx1[5] == cabx2[1] + cabx2[3] or cabx1[1] + cabx1[5] == cabx2[1] + cabx2[4]
    		  	or cabx1[1] + cabx1[6] == cabx2[1] + cabx2[3] or cabx1[1] + cabx1[6] == cabx2[1] + cabx2[4]) :
                distance = 0
            else:
                min1 = min(abs(cabx1[0] + cabx1[2] - cabx2[0]), abs(cabx2[0] + cabx2[2] - cabx1[0]))
                min2 = min(abs(cabx1[1] + cabx1[3] - cabx2[1] - cabx2[5]), abs(cabx1[1] + cabx1[3] - cabx2[1] - cabx2[6]))
                min3 = min(abs(cabx1[1] + cabx1[4] - cabx2[1] - cabx2[5]), abs(cabx1[1] + cabx1[4] - cabx2[1] - cabx2[6]))
                min4 = min(abs(cabx1[1] + cabx1[5] - cabx2[1] - cabx2[3]), abs(cabx1[1] + cabx1[5] - cabx2[1] - cabx2[4]))
                min5 = min(abs(cabx1[1] + cabx1[6] - cabx2[1] - cabx2[3]), abs(cabx1[1] + cabx1[6] - cabx2[1] - cabx2[4]))
                Min = min(min1, min2, min3, min4, min5)
                distance= Min
        return distance
    
    def deckOffset(self, xcab, deckShape):
        """Retruns the hull offset at a given longitudinal position and deck height"""
        deckoffset = 0
        for sec in range(len(deckShape[0])-1):
            if deckShape[0][sec]>= xcab and deckShape[0][sec+1] <= xcab:
                deckoffset = deckShape[1][sec] + (xcab - deckShape[0][sec])*((deckShape[1][sec+1] - deckShape[1][sec]) / (deckShape[0][sec+1] - deckShape[0][sec]))
        return deckoffset
        
    def deckShape(self, floor, linesPlanData, plot = False):
        """Returns the deck shape coordianates (x,y) at deck height"""
        deckLength = abs(linesPlanData[0][0][0]-linesPlanData[-1][0][0])
        self.deckLength = deckLength
        x = []
        y = []
        f = 1 # amplification
        x.append(self.LOA*f)
        y.append(interpol_section_points(linesPlanData[0],floor))
        for sec in range(len(linesPlanData)):
            x.append(abs(linesPlanData[sec][0][0]-0.5*deckLength)*f)
            y.append(interpol_section_points(linesPlanData[sec],floor))
        x.append(0)
        y.append(0)
        deck = np.array([x,y])
        self.deckshape = deck
        self.specs.update({'Deck height': floor})
        if plot:
            y = np.array(y)
            plt.plot(x,y, 'k')
            plt.plot(x,-y,'k')
            plt.axis('equal')
        return (deck)

    def intergal(self, upperLim, lowerLim, nbpoint):
        step = (upperLim-lowerLim)/nbpoint
        area = 0
        for i in range(nbpoint):
            area += step*self.deckShape(lowerLim+((i+0.5)*step))
        return area

        
    """**** Layout Constraints  ****"""

    def overLapConst(self, cabnum):
        """ Take cabin index and returns a list of the minimum changes of dimensions 
        required to avoid overlap in each direction (4 conditions)
        This is used to then adjust/shrink the cabin to avoid overlap"""
        cab_i_x = self.variables[cabnum]
        c1 = [0]
        c2 = [0]
        c3 = [0]
        c4 = [0]
        nums = list(range(0,self.nbCab))
        for j in nums:
            if j !=cabnum:
                cab_j_x = self.variables[j]
                condition1 = cab_i_x[0] + cab_i_x[2] - cab_j_x[0]#long 
                condition2 = ((cab_i_x[1]) + max(cab_i_x[4], cab_i_x[3]) - (cab_j_x[1])+max(abs(cab_j_x[5]),abs(cab_j_x[6])))#trans
                condition3 = -((cab_i_x[1])-max(abs(cab_i_x[5]),abs(cab_i_x[6])) - max(cab_j_x[4], cab_j_x[3]) - (cab_j_x[1]))#trans
                condition4 = -(cab_i_x[0] - cab_j_x[2] - cab_j_x[0])#long 
                if (round(condition1,4) <=0  or round(condition2,4) <= 0 or round(condition3,4) <= 0 or round(condition4, 4) <= 0 ): pass
                else:
                    if cabnum < j :
                        minov = min(condition1, condition2, condition3, condition4)
                        if cab_j_x[0]<=cab_i_x[0]<=cab_i_x[0] + cab_i_x[2]<=cab_j_x[2]+cab_j_x[0]:
                            if cab_i_x[1] >= cab_j_x[1]: minov = condition3
                            if cab_j_x[1]>cab_i_x[1]: minov = condition2
                        elif (cab_i_x[0]<=cab_j_x[0]<=cab_j_x[0] + cab_j_x[2]<=cab_i_x[2]+cab_i_x[0]): 
                            minov = condition1
                        elif ((cab_i_x[1]+max(cab_i_x[4], cab_i_x[3])<cab_j_x[1]+max(cab_j_x[4], cab_j_x[3]))
                        and (cab_i_x[1]+min(cab_i_x[5], cab_i_x[6])>cab_j_x[1]+min(cab_j_x[5], cab_j_x[6]))):
                            minov = min(condition1, condition4)
                        elif  (min(condition1, condition2, condition3, condition4) == condition3 
                               and cab_i_x[1]<cab_j_x[1]+max(cab_j_x[3],cab_j_x[4])):
                            minov = min(condition1, condition4)
                        elif  (min(condition1, condition2, condition3, condition4) == condition2
                               and cab_i_x[1]>cab_j_x[1]+min(cab_j_x[5],cab_j_x[6])):
                            minov = min(condition1, condition4)
                    else:
                        minov = min(condition1, condition2, condition3)
                        if cab_j_x[0]<=cab_i_x[0]<=cab_i_x[0] + cab_i_x[2]<=cab_j_x[2]+cab_j_x[0]:
                            if cab_i_x[1] >= cab_j_x[1]: minov = condition3
                            if cab_j_x[1]>cab_i_x[1]: minov = condition2
                        elif (cab_i_x[0]<=cab_j_x[0]<=cab_j_x[0] + cab_j_x[2]<=cab_i_x[2]+cab_i_x[0]): 
                            minov = condition1
                        elif ((cab_i_x[1]+max(cab_i_x[4], cab_i_x[3])<cab_j_x[1]+max(cab_j_x[4], cab_j_x[3]))
                        and (cab_i_x[1]+min(cab_i_x[5], cab_i_x[6])>cab_j_x[1]+min(cab_j_x[5], cab_j_x[6]))):
                            minov = condition1
                        elif  minov == condition3 and cab_i_x[1]<cab_j_x[1]+max(cab_j_x[3],cab_j_x[4]):
                            minov = condition1
                        elif  minov == condition2 and cab_i_x[1]>cab_j_x[1]+min(cab_j_x[5],cab_j_x[6]):
                            minov = condition1
                    if minov == condition1: c1.append(minov)
                    if minov == condition2: c2.append(minov)
                    if minov == condition3: c3.append(minov)
                    if minov == condition4: c4.append(minov)                   
        delta_L = max(c1)
        delta_Wu = max(c2)
        delta_Wb = max(c3)
        delta_x = max(c4)
        return(delta_L, delta_Wu,delta_Wb, delta_x)


#    def distance(self, cabidx):
#        centroids = []
#        for cab in cabidx:
#            if cab == 'aft':
#                centroids.append((1*self.LOA, 0))
#            elif cab == 'stairs':
#                centroids.append((self.LOA, 0))
#            else:  
#                for c in self.cabType:
#                    if cab == c: 
#                        idx = self.cabType.index(cab)
#                        cabx = self.cabinVarSort(idx)
#                        C_i = findCentroid(cabx)
#                        centroids.append(C_i)
#                        #plt.plot(C_i[0],C_i[1], 'wo')
#                    else: pass 
#        if len(centroids) == 2:
#            c_1 = centroids[0]
#            c_2 = centroids[1]
#            dist = calcDistance(c_1, c_2)
#        else:  dist = 0
##            dist = abs(xtarg - C)*999/self.nbCab
#        return  dist
        
    def distance(self, cabidx):
        """ return the distance between 2 cabins.
        Takes as input a list with 2 arguments corresponding to cabin indeces."""
        centroids = []
        for cab in cabidx:
            if cab == "aft":
                centroids.append(-1)
#            elif cab == 'fore':
#                centroids.append(-2)
            else:  
                for c in self.cabType:
                    if cab == c: 
                        idx = self.cabType.index(cab)
                        centroids.append(idx)
                    else: pass 
        if len(centroids) == 2:
            c_1 = centroids[0]
            c_2 = centroids[1]
            dist = self.calcDistanceIndex(c_1, c_2)
        else:  dist = 0
        return  dist    

    """**** Layout Fitness(es)  ****"""

    def spaceFitness(self):
        areaUsed = 0
        deckArea = -2*(integral_trap(self.deckshape[0],self.deckshape[1]))
        self.deckArea = deckArea
        self.specs.update({'Deck Area': self.deckArea})
        for n in range(0,self.nbCab):
            cabx = self.cabinVarSort(n)
            area_cab = cabinArea(cabx)
            areaUsed += area_cab 
            self.specs.update({'{} area'.format(self.cabType[n]): area_cab})
        spaceLoss = (deckArea - areaUsed)/deckArea
        if spaceLoss <0:
            spaceLoss = -spaceLoss
        else:
            spaceLoss = spaceLoss
        self.spaceLoss = spaceLoss
        return spaceLoss
    
    def distFit(self):
        cabs = self.cabinFlow
        distances = 0
        for couple in cabs:
            dist = self.distance(couple)
            distances += dist
        dist_Const = distances/(self.nbCab)
        return dist_Const
    
    def cabinsizeConst(self):
        """ Compares the actual area of each cabin to their standards
        industry sizes. A 20% margin is applied. The function returns the
        total penalty"""
        UB = 1.2
        LB = 0.8
        sizecab = []
        for i in range(len(self.cabsizes)):
             cabx = self.cabinVarSort(i)
             area = cabinArea(cabx)
             mina = LB * self.cabsizes[i]
             sizecab.append(mina - area)
             maxa = UB * self.cabsizes[i]
             sizecab.append(area - maxa)
        CSC = 0
        for s in range(len(sizecab)):
            if sizecab[s]>0:
                CSC += sizecab[s]
        return CSC/(self.nbCab*self.LOA)
    
    def CG(self):
        longi_moment = 0
        transverse_moment = 0
        for i in range(0,self.nbCab):
            cabx = self.cabinVarSort(i)
            mass_cab_i = self.cabmass[i]*cabinArea(cabx)
            self.cabmass[i] = mass_cab_i
            Cg_cab_i = findCentroid(cabx)
            Lcg = Cg_cab_i[0]
            Tcg = Cg_cab_i[1]
            longi_moment += Lcg*mass_cab_i
            transverse_moment += Tcg*mass_cab_i
        totalcabmass = sum(self.cabmass)
        LCG = longi_moment/totalcabmass
        LCB_midship,TCB = CB(self.linesPlan,self.wl)
        LCB = abs(LCB_midship-0.5*self.LOA)
        TCG = transverse_moment/totalcabmass
        C_G = (LCG, TCG)
        C_B = (LCB,TCB)
        Distance = calcDistance(C_B,C_G)/self.LOA
        return Distance
    
    """**** Layout plot designs  ****"""
    def plotdeck(self, save = False):
        plt.figure()
        self.deckShape(self.floor, self.linesPlan, True)
        colors = ['orange', 'blue', 'black', 'red', 'yellow','green', 'grey', 'aqua', 'purple', 'salmon','seagreen','brown','pink','peru','olive','darkcyan', 'fuchsia','tomato','slategray','orange', 'blue', 'black', 'red', 'yellow','green', 'grey', 'aqua', 'purple', 'salmon','seagreen','brown','pink','peru','olive','darkcyan', 'fuchsia','tomato','slategray','black', 'red', 'yellow','green', 'grey', 'aqua', 'purple', 'salmon','seagreen']
        for i in (range((self.nbCab))):
            cabx = self.variables[i]
            a = np.array([cabx[0], cabx[0]+cabx[2]])
            b = np.array([cabx[1], cabx[1]])
            '********************'
            hn = np.array([cabx[1]+cabx[3], cabx[1]+cabx[4]])
            po = np.array([cabx[1]+cabx[5], cabx[1]+cabx[6]])
            plt.fill_between(a,b,hn, color = colors[i], label = self.cabType[i])
            plt.fill_between(a,b,po, color = colors[i])
            '********************'
        for bulkhead in self.bulkheads:
            plt.axvline(x = bulkhead, ymin = 00.25, ymax = 0.75, color = 'k')
        if len(self.corridor) != 0:
            if self.deck_number>0: lim = self.LOA
            else : lim = self.bulkheads[1]
            for i in range(len(self.corridor)):
                plt.plot([0,lim],[self.corridor[i], self.corridor[i]], 'k')
        plt.axis('equal')
        plt.legend(loc = 9,bbox_to_anchor = (1.2,1), fontsize=8)
        plt.xlabel("Longitudinal position (m)")
        plt.ylabel("Transverse position (m)")
        date = str(datetime.now())
        date = date.replace(' ','_')
        date = date.replace('-','_')
        date = date.replace(':','_')
        date = date.replace('.','_')
        if save:
            plt.savefig('E:/4th Year/GDP/Validation/Images_sun/'+date,bbox_inches='tight')#'.jpg')
            plt.clf()
        plt.show()
        
    def calculateDeck(self, deck_floor, plot = False):
       spaceFit = self.spaceFitness()
       cabsizeconst = self.cabinsizeConst()
       distFit = self.distFit()
       cgFit = self.CG()
       if plot :
           self.plotdeck()
       return (spaceFit,distFit,cabsizeconst,cgFit)#, cabsizeconst)#, deckconst,cabsizeconst, bulkheadConst)

