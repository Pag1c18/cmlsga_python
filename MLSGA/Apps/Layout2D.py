# -*- coding: utf-8 -*-
"""
Created on Sun Oct 21 19:47:56 2018

@author: Thomas
"""

from Layout import TwoDlayout
from Inputs import *
import matplotlib.pyplot as plt
from generalFunction import *
import numpy as np
import warnings
warnings.simplefilter("ignore")


Hull  = linesPlanData(filename)
    
def Layout2D(var, decks, plot):
    fitness = []
    linesdata = Hull
    cabs = deck_sort_cabin(LOA, deck_nb, master_pos, double_bedroom_nb,single_bedroom_nb,masters_bedroom_nb,crew_room_nb,crew_recreational_area_nb,saloon_nb,galley_nb,dining_room_nb, office_nb,bridge_nb,gym_nb,tender_nb,machinery)
    for deck in decks:
        idx = decks.index(deck)
        cabins = cabs[idx]
        nbcab = (len(cabins.columns))
        if idx == 0:
            cabvar = var[:nbcab*7]
        if idx == 1:
            nbcab_old = len(cabs[idx-1].columns)
            cabvar = var[nbcab_old*7:nbcab_old*7+nbcab*7]
        corridors = []
        [corridors.append(var[-(i+1)]) for i in range(corridor_nb)]
        Layout = TwoDlayout (cabvar,corridors,LOA, cabins,idx, cabinFlow, deck, bk, linesdata, waterline)
        fit = Layout.calculateDeck(deck, plot=False)
        [fitness.append(fit[i]) for i in range(len(fit))]
    return fitness


