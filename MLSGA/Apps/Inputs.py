# -*- coding: utf-8 -*-
"""
Created on Thu Feb 28 11:17:25 2019

@author: tdvs1g15
"""
"""Length"""


LOA = 140  #25#50#140




"""decks"""
if LOA == 140:
    cabinFlow = [['Double room 2', "Double room 1"],['Double room 2', "Double room 5"],['Double room 3', "Double room 1"],['Double room 3', "Double room 4"],['Double room 3', "Double room 6"],['Single room 1', "Single room 2"],['Single room 2', "Single room 3"],['Single room 3', "Single room 4"],['Single room 4', "Single room 5"],['Single room 5', "Single room 6"],['Single room 5', "Single room 3"],['Single room 7', "Single room 2"],['Machinery', 'aft'],['Tender', 'aft']]
    deck_nb = 2
    decks = [10,13]
    corridor_nb = 2
    waterline = 5.
    bk = [0.82,0.65,0.48,0.3,0.1]
    filename = "Yacht_data_150m.csv"
    double_bedroom_nb = 6
    single_bedroom_nb = 7
    masters_bedroom_nb = 1
    master_pos = 2
    crew_room_nb = round(0.5*(0.458*LOA-9.409)) 
    saloon_nb = 3
    galley_nb = 3
    dining_room_nb = 2
    office_nb = 2
    bridge_nb = 1
    gym_nb = 1
    tender_nb = 1
    machinery = 1

if LOA == 25:
     cabinFlow = [['Double room 2', "Double room 1"],['Double room 3', "Double room 1"],['Double room 2', "Double room 3"],['Crew cabin 1', 'aft'],['Machinery', 'aft']]
     deck_nb = 1
     decks = [4.1]
     corridor_nb = 0
     waterline = 2
     bk = [0.90,0.725,0.50,0.08]
     filename = "Yacht_data.csv"
     double_bedroom_nb = 3
     single_bedroom_nb = 0
     masters_bedroom_nb = 1
     master_pos = 1
     crew_room_nb = 1
     saloon_nb = 0
     galley_nb = 0
     dining_room_nb = 0
     office_nb = 0
     bridge_nb = 0
     gym_nb = 0
     tender_nb = 0
     machinery = 1
     
if LOA == 50:
    cabinFlow = [['Double room 2', "Double room 1"],['Double room 2', "Double room 2"],['Double room 2', "Double room 1"],['Double room 2', "Double room 3"],['Double room 3', "Double room 4"],['Dining room 1', 'Galley 1'],['Dining room 1', 'Saloon 1']]
    deck_nb = 1
    decks = [6.6]
    corridor_nb = 0
    waterline = 3.5
    bk = [0.80,0.60,0.45,0.25,0.1]
    filename = "Yacht_data_50m.csv"
    double_bedroom_nb = 5
    single_bedroom_nb = 0
    masters_bedroom_nb = 1
    master_pos = 1
    crew_room_nb = 0
    saloon_nb = 1
    galley_nb = 1
    dining_room_nb = 1
    office_nb = 0
    bridge_nb = 0
    gym_nb = 1
    tender_nb = 0
    machinery = 0

if 10>crew_room_nb>=5: crew_recreational_area_nb = 1
elif  crew_room_nb>=10: crew_recreational_area_nb = 2
else: crew_recreational_area_nb = 0

inputs = [
    double_bedroom_nb,
    single_bedroom_nb,
    masters_bedroom_nb,
    crew_room_nb,
    crew_recreational_area_nb,
    saloon_nb,
    galley_nb,
    dining_room_nb,
    office_nb,
    bridge_nb,
    gym_nb,                  
    tender_nb,                   
    machinery,
]
nb = sum(inputs)
obj_nb = 4*deck_nb