import math
import parameters

class Function(object):
    """description of class"""
    def __init__(self, fname, varsn, objsn, consn):
        self.name_func = fname;
        self.num_vars = varsn;
        self.num_objs = objsn;
        self.num_cons = consn;
        self.bound_min = [];
        self.bound_max = [];


class UF1(Function):
    def __init__(self):
        Function.__init__(self,"UF1",30,2,0)
        self.Bound_Set();

    def Bound_Set(self):
        self.bound_max = [1];
        self.bound_min = [0];
        for i in range(1,self.num_vars):
            self.bound_max.append(1);
            self.bound_min.append(-1);

    def Fitness_C(self, code):
        SumJ1 = 0.;
        SumJ2 = 0.;
        sizeJ1 = 0;
        sizeJ2 = 0;

        for i in range (2, self.num_vars + 1):
            y = code[i - 1] - math.sin((6. * parameters.pi * code[0]) + (i * parameters.pi) / self.num_vars)
            if (i % 2 == 1):
                sizeJ1 += 1;
                SumJ1 += pow(y, 2)
            else:
                sizeJ2 +=1;
                SumJ2 += pow(y, 2)
        
        fitness1 = code[0] + (2. / sizeJ1)*SumJ1;
        fitness2 = 1. - math.sqrt(code[0]) + (2. / sizeJ2)*SumJ2;

        fitness = [fitness1,fitness2];

        return fitness;

    def Plot_PF(self,size):
        Real_PF = [];

        for i in range (0,size):
            temp_f1 = i/(size-1.);
            temp_f2 = 1- math.sqrt(temp_f1);

            PF_val = [temp_f1, temp_f2]
            Real_PF.append(list(PF_val));

        return Real_PF



class UF2(Function):
    def __init__(self):
        Function.__init__(self,"UF2",30,2,0)
        self.Bound_Set();

    def Bound_Set(self):
        self.bound_max = [1];
        self.bound_min = [0];
        for i in range(1,self.num_vars):
            self.bound_max.append(1);
            self.bound_min.append(-1);
    def Fitness_C(self, code):
        SumJ1 = 0.;
        SumJ2 = 0.;
        sizeJ1 = 0;
        sizeJ2 = 0;

        for i in range (2, self.num_vars + 1):
            if (i % 2 == 1):
                sizeJ1 += 1;
                SumJ1 += pow(code[i-1] - (0.3*pow(code[0], 2)*math.cos(24 * parameters.pi*code[0] + 4. * i*parameters.pi / self.num_vars) + 0.6*code[0])*math.cos(6 * parameters.pi*code[0] + i*parameters.pi / self.num_vars), 2);
            else:
                sizeJ2 +=1;
                SumJ2 += pow(code[i-1] - (0.3*pow(code[0], 2)*math.cos(24 * parameters.pi*code[0] + 4. * i*parameters.pi / self.num_vars) + 0.6*code[0])*math.sin(6 * parameters.pi*code[0] + i*parameters.pi / self.num_vars), 2);
        
        fitness1 = code[0] + (2. / sizeJ1)*SumJ1;
        fitness2 = 1. - code[0]**0.5 + (2. / sizeJ2)*SumJ2;

        fitness = [fitness1,fitness2];

        return fitness;

    def Plot_PF(self,size):
        Real_PF = [];

        for i in range (0,size):
            temp_f1 = i/(size-1.);
            temp_f2 = 1- math.sqrt(temp_f1);

            PF_val = [temp_f1, temp_f2]
            Real_PF.append(list(PF_val));

        return Real_PF